// Param values from https://developer.mozilla.org/Add-ons/WebExtensions/API/contextualIdentities/create
const EBAY_CONTAINER_NAME = "Ebay";
const EBAY_CONTAINER_COLOR = "green";
const EBAY_CONTAINER_ICON = "briefcase";
const EBAY_DOMAINS = [
	"ebay.at", "anywhere.ebay.at", "apibulk.ebay.at", "applications.ebay.at",
	"arbd.ebay.at", "artist-index.ebay.at", "auth.ebay.at", "blogs.ebay.at",
	"bulksell.ebay.at", "catalog.ebay.at", "cgi.ebay.at", "cgi1.ebay.at",
	"cgi2.ebay.at", "cgi3.ebay.at", "cgi4.ebay.at", "cgi5.ebay.at",
	"cgi6.ebay.at", "community.ebay.at", "contact.ebay.at", "csr.ebay.at",
	"epn.ebay.at", "events.ebay.at", "feedback.ebay.at", "fyp.ebay.at",
	"hub.ebay.at", "item.ebay.at", "k2b-bulk.ebay.at", "listings.ebay.at",
	"m.ebay.at", "members.ebay.at", "mesg.ebay.at", "mesgmy.ebay.at",
	"motors.ebay.at", "my.ebay.at", "myworld.ebay.at", "ocs.ebay.at",
	"ocsnext.ebay.at", "offer.ebay.at", "pages.ebay.at", "pay.ebay.at",
	"payments.ebay.at", "portal.ebay.at", "postorder.ebay.at",
	"pulsar.ebay.at", "pulse.ebay.at", "qu.ebay.at", "reg.ebay.at",
	"res.ebay.at", "resolutioncenter.ebay.at", "rover.ebay.at", "scgi.ebay.at",
	"search-completed.ebay.at", "sell.ebay.at", "sellerstandards.ebay.at",
	"shop.ebay.at", "signin.ebay.at", "signinalert.ebay.at", "stores.ebay.at",
	"svcs.ebay.at", "viv.ebay.at", "vod.ebay.at", "wantitnow.ebay.at",
	"wap.ebay.at", "www.ebay.at", "ebay.be", "anywhere.ebay.be",
	"applications.ebay.be", "auth.ebay.be", "bulksell.ebay.be", "epn.ebay.be",
	"mesg.ebay.be", "pages.ebay.be", "payments.ebay.be", "portal.ebay.be",
	"res.ebay.be", "signin.ebay.be", "signinalert.ebay.be", "www.ebay.be",
	"befr.ebay.be", "anywhere.befr.ebay.be", "apibulk.befr.ebay.be",
	"arbd.befr.ebay.be", "artist-index.befr.ebay.be", "auth.befr.ebay.be",
	"bulksell.befr.ebay.be", "catalog.befr.ebay.be", "cgi.befr.ebay.be",
	"cgi1.befr.ebay.be", "cgi2.befr.ebay.be", "cgi3.befr.ebay.be",
	"cgi4.befr.ebay.be", "cgi5.befr.ebay.be", "cgi6.befr.ebay.be",
	"contact.befr.ebay.be", "csr.befr.ebay.be", "feedback.befr.ebay.be",
	"fyp.befr.ebay.be", "hub.befr.ebay.be", "item.befr.ebay.be",
	"k2b-bulk.befr.ebay.be", "listings.befr.ebay.be", "m.befr.ebay.be",
	"members.befr.ebay.be", "mesg.befr.ebay.be", "mesgmy.befr.ebay.be",
	"motors.befr.ebay.be", "my.befr.ebay.be", "myworld.befr.ebay.be",
	"ocs.befr.ebay.be", "ocsnext.befr.ebay.be", "offer.befr.ebay.be",
	"pages.befr.ebay.be", "pay.befr.ebay.be", "payments.befr.ebay.be",
	"postorder.befr.ebay.be", "pulsar.befr.ebay.be", "pulse.befr.ebay.be",
	"qu.befr.ebay.be", "reg.befr.ebay.be", "res.befr.ebay.be",
	"resolutioncenter.befr.ebay.be", "rover.befr.ebay.be", "scgi.befr.ebay.be",
	"search-completed.befr.ebay.be", "sell.befr.ebay.be",
	"sellerstandards.befr.ebay.be", "services.befr.ebay.be",
	"shop.befr.ebay.be", "signin.befr.ebay.be", "stores.befr.ebay.be",
	"svcs.befr.ebay.be", "viv.befr.ebay.be", "wantitnow.befr.ebay.be",
	"wap.befr.ebay.be", "www.befr.ebay.be", "benl.ebay.be",
	"anywhere.benl.ebay.be", "apibulk.benl.ebay.be", "arbd.benl.ebay.be",
	"artist-index.benl.ebay.be", "auth.benl.ebay.be", "bulksell.benl.ebay.be",
	"catalog.benl.ebay.be", "cgi.benl.ebay.be", "cgi1.benl.ebay.be",
	"cgi2.benl.ebay.be", "cgi3.benl.ebay.be", "cgi4.benl.ebay.be",
	"cgi5.benl.ebay.be", "cgi6.benl.ebay.be", "contact.benl.ebay.be",
	"csr.benl.ebay.be", "feedback.benl.ebay.be", "fyp.benl.ebay.be",
	"hub.benl.ebay.be", "item.benl.ebay.be", "k2b-bulk.benl.ebay.be",
	"listings.benl.ebay.be", "m.benl.ebay.be", "members.benl.ebay.be",
	"mesg.benl.ebay.be", "mesgmy.benl.ebay.be", "motors.benl.ebay.be",
	"my.benl.ebay.be", "myworld.benl.ebay.be", "ocs.benl.ebay.be",
	"ocsnext.benl.ebay.be", "offer.benl.ebay.be", "pages.benl.ebay.be",
	"pay.benl.ebay.be", "payments.benl.ebay.be", "postorder.benl.ebay.be",
	"pulsar.benl.ebay.be", "pulse.benl.ebay.be", "qu.benl.ebay.be",
	"reg.benl.ebay.be", "res.benl.ebay.be", "resolutioncenter.benl.ebay.be",
	"rover.benl.ebay.be", "scgi.benl.ebay.be", "search-completed.benl.ebay.be",
	"sell.benl.ebay.be", "sellerstandards.benl.ebay.be",
	"services.benl.ebay.be", "shop.benl.ebay.be", "signin.benl.ebay.be",
	"stores.benl.ebay.be", "svcs.benl.ebay.be", "viv.benl.ebay.be",
	"wantitnow.benl.ebay.be", "wap.benl.ebay.be", "www.benl.ebay.be",
	"ebay.ca", "affiliates.ebay.ca", "announcements.ebay.ca",
	"anywhere.ebay.ca", "apibulk.ebay.ca", "applications.ebay.ca",
	"arbd.ebay.ca", "artist-index.ebay.ca", "auth.ebay.ca",
	"autodiscover.ebay.ca", "blogs.ebay.ca", "bulksell.ebay.ca",
	"catalog.ebay.ca", "cgi.ebay.ca", "cgi1.ebay.ca", "cgi2.ebay.ca",
	"cgi3.ebay.ca", "cgi4.ebay.ca", "cgi5.ebay.ca", "cgi6.ebay.ca",
	"coins.ebay.ca", "community.ebay.ca", "contact.ebay.ca", "csr.ebay.ca",
	"deals.ebay.ca", "epn.ebay.ca", "feedback.ebay.ca", "fyp.ebay.ca",
	"green.ebay.ca", "hub.ebay.ca", "instantsale.ebay.ca",
	"instantsale.ebay.ca", "item.ebay.ca", "k2b-bulk.ebay.ca",
	"listings.ebay.ca", "m.ebay.ca", "members.ebay.ca", "mesg.ebay.ca",
	"mesgmy.ebay.ca", "mmm.ebay.ca", "mobile.ebay.ca", "motors.ebay.ca",
	"my.ebay.ca", "myworld.ebay.ca", "ocs.ebay.ca", "ocsnext.ebay.ca",
	"offer.ebay.ca", "pages.ebay.ca", "pay.ebay.ca", "payments.ebay.ca",
	"popular.ebay.ca", "portal.ebay.ca", "postorder.ebay.ca", "pulsar.ebay.ca",
	"pulse.ebay.ca", "qu.ebay.ca", "reg.ebay.ca", "res.ebay.ca",
	"resolutioncenter.ebay.ca", "rover.ebay.ca", "scgi.ebay.ca",
	"search-completed.ebay.ca", "sell.ebay.ca", "sellerstandards.ebay.ca",
	"services.ebay.ca", "shop.ebay.ca", "signin.ebay.ca",
	"signinalert.ebay.ca", "stores.ebay.ca", "svcs.ebay.ca", "viv.ebay.ca",
	"vod.ebay.ca", "wantitnow.ebay.ca", "wap.ebay.ca", "www.ebay.ca",
	"cafr.ebay.ca", "affiliates.cafr.ebay.ca", "announcements.cafr.ebay.ca",
	"anywhere.cafr.ebay.ca", "apibulk.cafr.ebay.ca",
	"applications.cafr.ebay.ca", "arbd.cafr.ebay.ca",
	"artist-index.cafr.ebay.ca", "auth.cafr.ebay.ca", "blogs.cafr.ebay.ca",
	"bulksell.cafr.ebay.ca", "catalog.cafr.ebay.ca", "cgi.cafr.ebay.ca",
	"cgi1.cafr.ebay.ca", "cgi2.cafr.ebay.ca", "cgi3.cafr.ebay.ca",
	"cgi4.cafr.ebay.ca", "cgi5.cafr.ebay.ca", "cgi6.cafr.ebay.ca",
	"coins.cafr.ebay.ca", "community.cafr.ebay.ca", "contact.cafr.ebay.ca",
	"csr.cafr.ebay.ca", "deals.cafr.ebay.ca", "feedback.cafr.ebay.ca",
	"fyp.cafr.ebay.ca", "hub.cafr.ebay.ca", "instantsale.cafr.ebay.ca",
	"instantsale.cafr.ebay.ca", "item.cafr.ebay.ca", "k2b-bulk.cafr.ebay.ca",
	"listings.cafr.ebay.ca", "m.cafr.ebay.ca", "members.cafr.ebay.ca",
	"mesg.cafr.ebay.ca", "mesgmy.cafr.ebay.ca", "mmm.cafr.ebay.ca",
	"mobile.cafr.ebay.ca", "motors.cafr.ebay.ca", "my.cafr.ebay.ca",
	"myworld.cafr.ebay.ca", "ocs.cafr.ebay.ca", "ocsnext.cafr.ebay.ca",
	"offer.cafr.ebay.ca", "pages.cafr.ebay.ca", "pay.cafr.ebay.ca",
	"payments.cafr.ebay.ca", "popular.cafr.ebay.ca", "postorder.cafr.ebay.ca",
	"pulsar.cafr.ebay.ca", "pulse.cafr.ebay.ca", "qu.cafr.ebay.ca",
	"reg.cafr.ebay.ca", "res.cafr.ebay.ca", "resolutioncenter.cafr.ebay.ca",
	"rover.cafr.ebay.ca", "scgi.cafr.ebay.ca", "search-completed.cafr.ebay.ca",
	"sell.cafr.ebay.ca", "sellerstandards.cafr.ebay.ca",
	"services.cafr.ebay.ca", "shop.cafr.ebay.ca", "signin.cafr.ebay.ca",
	"signinalert.cafr.ebay.ca", "stores.cafr.ebay.ca", "svcs.cafr.ebay.ca",
	"viv.cafr.ebay.ca", "vod.cafr.ebay.ca", "wantitnow.cafr.ebay.ca",
	"wap.cafr.ebay.ca", "www.cafr.ebay.ca", "ebay.ch", "anywhere.ebay.ch",
	"apibulk.ebay.ch", "applications.ebay.ch", "arbd.ebay.ch",
	"artist-index.ebay.ch", "auth.ebay.ch", "blogs.ebay.ch",
	"bulksell.ebay.ch", "catalog.ebay.ch", "cgi.ebay.ch", "cgi1.ebay.ch",
	"cgi2.ebay.ch", "cgi3.ebay.ch", "cgi4.ebay.ch", "cgi5.ebay.ch",
	"cgi6.ebay.ch", "community.ebay.ch", "contact.ebay.ch", "csr.ebay.ch",
	"epn.ebay.ch", "feedback.ebay.ch", "fyp.ebay.ch", "hub.ebay.ch",
	"item.ebay.ch", "k2b-bulk.ebay.ch", "listings.ebay.ch", "m.ebay.ch",
	"members.ebay.ch", "mesg.ebay.ch", "mesgmy.ebay.ch", "motors.ebay.ch",
	"my.ebay.ch", "myworld.ebay.ch", "ocs.ebay.ch", "ocsnext.ebay.ch",
	"offer.ebay.ch", "pages.ebay.ch", "pay.ebay.ch", "payments.ebay.ch",
	"portal.ebay.ch", "postorder.ebay.ch", "pulsar.ebay.ch", "pulse.ebay.ch",
	"qu.ebay.ch", "reg.ebay.ch", "res.ebay.ch", "resolutioncenter.ebay.ch",
	"rover.ebay.ch", "scgi.ebay.ch", "search-completed.ebay.ch",
	"sell.ebay.ch", "sellerstandards.ebay.ch", "shop.ebay.ch",
	"signin.ebay.ch", "signinalert.ebay.ch", "stores.ebay.ch", "svcs.ebay.ch",
	"viv.ebay.ch", "wantitnow.ebay.ch", "www.ebay.ch", "ebay.cn",
	"app.ebay.cn", "community.ebay.cn", "epn.ebay.cn", "my.ebay.cn",
	"news.ebay.cn", "pages.ebay.cn", "qu.ebay.cn", "search.ebay.cn",
	"svcs.ebay.cn", "wap.ebay.cn", "www.ebay.cn", "ebay.co.kr",
	"www.ebay.co.kr", "ebay.co.uk", "affiliates.ebay.co.uk",
	"anywhere.ebay.co.uk", "apibulk.ebay.co.uk", "applications.ebay.co.uk",
	"arbd.ebay.co.uk", "artist-index.ebay.co.uk", "auth.ebay.co.uk",
	"blogs.ebay.co.uk", "bulksell.ebay.co.uk", "catalog.ebay.co.uk",
	"cgi.ebay.co.uk", "cgi1.ebay.co.uk", "cgi2.ebay.co.uk", "cgi3.ebay.co.uk",
	"cgi4.ebay.co.uk", "cgi5.ebay.co.uk", "cgi6.ebay.co.uk",
	"charity.ebay.co.uk", "coins.ebay.co.uk", "community.ebay.co.uk",
	"contact.ebay.co.uk", "csr.ebay.co.uk", "epn.ebay.co.uk",
	"feedback.ebay.co.uk", "fyp.ebay.co.uk", "garden.ebay.co.uk",
	"hub.ebay.co.uk", "ie8.ebay.co.uk", "item.ebay.co.uk",
	"k2b-bulk.ebay.co.uk", "listings.ebay.co.uk", "m.ebay.co.uk",
	"members.ebay.co.uk", "mesg.ebay.co.uk", "mesgmy.ebay.co.uk",
	"mmm.ebay.co.uk", "mobile.ebay.co.uk", "motors.ebay.co.uk",
	"my.ebay.co.uk", "myworld.ebay.co.uk", "ocs.ebay.co.uk",
	"ocsnext.ebay.co.uk", "offer.ebay.co.uk", "pages.ebay.co.uk",
	"partnernetwork.ebay.co.uk", "pay.ebay.co.uk", "payments.ebay.co.uk",
	"popular.ebay.co.uk", "portal.ebay.co.uk", "postage.ebay.co.uk",
	"postorder.ebay.co.uk", "pulsar.ebay.co.uk", "pulse.ebay.co.uk",
	"qu.ebay.co.uk", "reg.ebay.co.uk", "res.ebay.co.uk", "reviews.ebay.co.uk",
	"rover.ebay.co.uk", "scgi.ebay.co.uk", "search-completed.ebay.co.uk",
	"sell.ebay.co.uk", "sellerstandards.ebay.co.uk", "shop.ebay.co.uk",
	"signin.ebay.co.uk", "signinalert.ebay.co.uk", "stores.ebay.co.uk",
	"svcs.ebay.co.uk", "viv.ebay.co.uk", "vod.ebay.co.uk",
	"wantitnow.ebay.co.uk", "wap.ebay.co.uk", "www.ebay.co.uk",
	"wwww.ebay.co.uk", "ebay.com", "adjustdiscount.ebay.com", "adn.ebay.com",
	"ads.ebay.com", "affiliates.ebay.com", "aka-msa-b1.ebay.com",
	"announcements.ebay.com", "anywhere.ebay.com", "apacshippingtool.ebay.com",
	"apibulk.ebay.com", "apics1.ebay.com", "app.ebay.com",
	"applications.ebay.com", "apps.ebay.com", "arbd.ebay.com",
	"artist-index.ebay.com", "att.ebay.com", "auth.ebay.com",
	"autodiscover.ebay.com", "bestof.ebay.com", "blog.ebay.com",
	"blogs.ebay.com", "brands.ebay.com", "bulksell.ebay.com",
	"catalog.ebay.com", "cc.ebay.com", "celebrity.ebay.com",
	"certifiedprovider.ebay.com", "cgi.ebay.com", "cgi1.ebay.com",
	"cgi2.ebay.com", "cgi3.ebay.com", "cgi4.ebay.com", "cgi5.ebay.com",
	"cgi6.ebay.com", "charity.ebay.com", "community.ebay.com",
	"contact.ebay.com", "countdown.ebay.com", "csr.ebay.com", "deals.ebay.com",
	"developer.ebay.com", "developer.ebay.com", "epn.ebay.com", "es.ebay.com",
	"feedback.ebay.com", "forums.ebay.com", "fyp.ebay.com", "garden.ebay.com",
	"giftcard.ebay.com", "givingworks.ebay.com", "global.ebay.com",
	"globaldeals.ebay.com", "green.ebay.com", "groupgifts.ebay.com",
	"groups.ebay.com", "haitao.ebay.com", "half.ebay.com", "hub.ebay.com",
	"ie8.ebay.com", "instantsale.ebay.com", "instantsale.ebay.com",
	"investor.ebay.com", "item.ebay.com", "k2b-bulk.ebay.com", "labs.ebay.com",
	"listing-index.ebay.com", "listings.ebay.com", "live.ebay.com",
	"m.ebay.com", "members.ebay.com", "mesg.ebay.com", "mesgmy.ebay.com",
	"mmm.ebay.com", "mobile.ebay.com", "my.ebay.com", "myworld.ebay.com",
	"neighborhoods.ebay.com", "news.ebay.com", "now.ebay.com", "ocs.ebay.com",
	"ocsnext.ebay.com", "offer.ebay.com", "pages.ebay.com",
	"partnernetwork.ebay.com", "pay.ebay.com", "payments.ebay.com",
	"play.ebay.com", "popular.ebay.com", "postage.ebay.com",
	"postorder.ebay.com", "pulsar.ebay.com", "qu.ebay.com", "r.ebay.com",
	"reg.ebay.com", "res.ebay.com", "resolutioncenter.ebay.com",
	"reviews.ebay.com", "rover.ebay.com", "sandbox.ebay.com", "scgi.ebay.com",
	"sea.ebay.com", "securethumbs.ebay.com", "sell.ebay.com",
	"sellerstandards.ebay.com", "sellforme.ebay.com", "sellitforward.ebay.com",
	"shop.ebay.com", "signin.ebay.com", "signinalert.ebay.com",
	"stores.ebay.com", "stylestories.ebay.com", "survey.ebay.com",
	"svcs.ebay.com", "tahiti.ebay.com", "tech.ebay.com", "twexport.ebay.com",
	"viv.ebay.com", "vod.ebay.com", "wantitnow.ebay.com", "wap.ebay.com",
	"worldofgood.ebay.com", "www.ebay.com", "www2.ebay.com", "wwww.ebay.com",
	"ebay.com.au", "affiliates.ebay.com.au", "anywhere.ebay.com.au",
	"apibulk.ebay.com.au", "applications.ebay.com.au", "arbd.ebay.com.au",
	"artist-index.ebay.com.au", "auth.ebay.com.au", "bestof.ebay.com.au",
	"blogs.ebay.com.au", "bulksell.ebay.com.au", "catalog.ebay.com.au",
	"cc.ebay.com.au", "cgi.ebay.com.au", "cgi1.ebay.com.au",
	"cgi2.ebay.com.au", "cgi3.ebay.com.au", "cgi4.ebay.com.au",
	"cgi5.ebay.com.au", "cgi6.ebay.com.au", "charity.ebay.com.au",
	"coins.ebay.com.au", "community.ebay.com.au", "contact.ebay.com.au",
	"csr.ebay.com.au", "deals.ebay.com.au", "epn.ebay.com.au",
	"feedback.ebay.com.au", "forums.ebay.com.au", "fyp.ebay.com.au",
	"hub.ebay.com.au", "ie8.ebay.com.au", "item.ebay.com.au",
	"k2b-bulk.ebay.com.au", "listings.ebay.com.au", "m.ebay.com.au",
	"members.ebay.com.au", "mesg.ebay.com.au", "mesgmy.ebay.com.au",
	"mmm.ebay.com.au", "motors.ebay.com.au", "my.ebay.com.au",
	"myworld.ebay.com.au", "ocs.ebay.com.au", "ocsnext.ebay.com.au",
	"offer.ebay.com.au", "pages.ebay.com.au", "partnernetwork.ebay.com.au",
	"pay.ebay.com.au", "payments.ebay.com.au", "play.ebay.com.au",
	"popular.ebay.com.au", "portal.ebay.com.au", "postage.ebay.com.au",
	"postorder.ebay.com.au", "pulsar.ebay.com.au", "pulse.ebay.com.au",
	"qu.ebay.com.au", "reg.ebay.com.au", "res.ebay.com.au",
	"reviews.ebay.com.au", "rover.ebay.com.au", "scgi.ebay.com.au",
	"search-completed.ebay.com.au", "sell.ebay.com.au",
	"sellerstandards.ebay.com.au", "services.ebay.com.au", "shop.ebay.com.au",
	"signin.ebay.com.au", "signinalert.ebay.com.au", "stores.ebay.com.au",
	"survey.ebay.com.au", "svcs.ebay.com.au", "tech.ebay.com.au",
	"viv.ebay.com.au", "vod.ebay.com.au", "wantitnow.ebay.com.au",
	"wap.ebay.com.au", "www.ebay.com.au", "wwww.ebay.com.au", "ebay.com.cn",
	"adjustdiscount.ebay.com.cn", "apibulk.ebay.com.cn", "artist.ebay.com.cn",
	"artist-index.ebay.com.cn", "attr-search.ebay.com.cn",
	"bulksell.ebay.com.cn", "cgi.ebay.com.cn", "cgi1.ebay.com.cn",
	"cgi2.ebay.com.cn", "cgi3.ebay.com.cn", "cgi4.ebay.com.cn",
	"cgi6.ebay.com.cn", "contact.ebay.com.cn", "feedback.ebay.com.cn",
	"forums.ebay.com.cn", "fyp.ebay.com.cn", "groups.ebay.com.cn",
	"hub.ebay.com.cn", "k2b-bulk.ebay.com.cn", "listings.ebay.com.cn",
	"members.ebay.com.cn", "my.ebay.com.cn", "myworld.ebay.com.cn",
	"ocsnext.ebay.com.cn", "offer.ebay.com.cn", "pages.ebay.com.cn",
	"payments.ebay.com.cn", "postorder.ebay.com.cn", "pulsar.ebay.com.cn",
	"pulse.ebay.com.cn", "res.ebay.com.cn", "reviews.ebay.com.cn",
	"rover.ebay.com.cn", "scgi.ebay.com.cn", "search.ebay.com.cn",
	"search-completed.ebay.com.cn", "search-desc.ebay.com.cn",
	"sell.ebay.com.cn", "signin.ebay.com.cn", "stores.ebay.com.cn",
	"viv.ebay.com.cn", "wantitnow.ebay.com.cn", "wap.ebay.com.cn",
	"www.ebay.com.cn", "ebay.com.hk", "anywhere.ebay.com.hk",
	"apibulk.ebay.com.hk", "applications.ebay.com.hk", "arbd.ebay.com.hk",
	"artist-index.ebay.com.hk", "auth.ebay.com.hk", "blogs.ebay.com.hk",
	"bulksell.ebay.com.hk", "catalog.ebay.com.hk", "cgi.ebay.com.hk",
	"cgi1.ebay.com.hk", "cgi2.ebay.com.hk", "cgi3.ebay.com.hk",
	"cgi4.ebay.com.hk", "cgi5.ebay.com.hk", "cgi6.ebay.com.hk",
	"contact.ebay.com.hk", "csr.ebay.com.hk", "epn.ebay.com.hk",
	"feedback.ebay.com.hk", "fyp.ebay.com.hk", "hub.ebay.com.hk",
	"item.ebay.com.hk", "k2b-bulk.ebay.com.hk", "listings.ebay.com.hk",
	"m.ebay.com.hk", "members.ebay.com.hk", "mesg.ebay.com.hk",
	"mesgmy.ebay.com.hk", "mobile.ebay.com.hk", "my.ebay.com.hk",
	"myworld.ebay.com.hk", "ocs.ebay.com.hk", "ocsnext.ebay.com.hk",
	"offer.ebay.com.hk", "pages.ebay.com.hk", "pay.ebay.com.hk",
	"payments.ebay.com.hk", "postorder.ebay.com.hk", "pulsar.ebay.com.hk",
	"qu.ebay.com.hk", "reg.ebay.com.hk", "res.ebay.com.hk",
	"resolutioncenter.ebay.com.hk", "rover.ebay.com.hk", "scgi.ebay.com.hk",
	"search-completed.ebay.com.hk", "sell.ebay.com.hk",
	"sellerstandards.ebay.com.hk", "shop.ebay.com.hk", "signin.ebay.com.hk",
	"signinalert.ebay.com.hk", "stores.ebay.com.hk", "svcs.ebay.com.hk",
	"viv.ebay.com.hk", "wantitnow.ebay.com.hk", "wap.ebay.com.hk",
	"www.ebay.com.hk", "ebay.cz", "eim.ebay.cz", "fyp.ebay.cz",
	"ocsnext.ebay.cz", "postorder.ebay.cz", "qu.ebay.cz", "rover.ebay.cz",
	"www.ebay.cz", "ebay.de", "anywhere.ebay.de", "apibulk.ebay.de",
	"applications.ebay.de", "arbd.ebay.de", "artist-index.ebay.de",
	"auth.ebay.de", "autodiscover.ebay.de", "blogs.ebay.de",
	"bulksell.ebay.de", "catalog.ebay.de", "cgi.ebay.de", "cgi1.ebay.de",
	"cgi2.ebay.de", "cgi3.ebay.de", "cgi4.ebay.de", "cgi5.ebay.de",
	"cgi6.ebay.de", "community.ebay.de", "contact.ebay.de", "csr.ebay.de",
	"epn.ebay.de", "feedback.ebay.de", "fyp.ebay.de", "hub.ebay.de",
	"ie8.ebay.de", "item.ebay.de", "k2b-bulk.ebay.de", "listings.ebay.de",
	"m.ebay.de", "members.ebay.de", "mesg.ebay.de", "mesgmy.ebay.de",
	"mmm.ebay.de", "mobile.ebay.de", "motors.ebay.de", "my.ebay.de",
	"myworld.ebay.de", "news.ebay.de", "ocs.ebay.de", "ocsnext.ebay.de",
	"offer.ebay.de", "pages.ebay.de", "partnernetwork.ebay.de", "pay.ebay.de",
	"payments.ebay.de", "portal.ebay.de", "postage.ebay.de",
	"postorder.ebay.de", "product.ebay.de", "pulsar.ebay.de", "pulse.ebay.de",
	"qu.ebay.de", "reg.ebay.de", "res.ebay.de", "resolutioncenter.ebay.de",
	"rover.ebay.de", "scgi.ebay.de", "search-completed.ebay.de",
	"search-desc.ebay.de", "sell.ebay.de", "sellerstandards.ebay.de",
	"shop.ebay.de", "signin.ebay.de", "signinalert.ebay.de", "stores.ebay.de",
	"survey.ebay.de", "svcs.ebay.de", "viv.ebay.de", "vod.ebay.de",
	"wantitnow.ebay.de", "wap.ebay.de", "www.ebay.de", "wwww.ebay.de",
	"ebay.es", "affiliates.ebay.es", "anywhere.ebay.es", "apibulk.ebay.es",
	"applications.ebay.es", "arbd.ebay.es", "artist-index.ebay.es",
	"auth.ebay.es", "autodiscover.ebay.es", "blogs.ebay.es",
	"bulksell.ebay.es", "catalog.ebay.es", "cgi.ebay.es", "cgi1.ebay.es",
	"cgi2.ebay.es", "cgi3.ebay.es", "cgi4.ebay.es", "cgi5.ebay.es",
	"cgi6.ebay.es", "contact.ebay.es", "csr.ebay.es", "epn.ebay.es",
	"feedback.ebay.es", "fyp.ebay.es", "hub.ebay.es", "item.ebay.es",
	"k2b-bulk.ebay.es", "listings.ebay.es", "m.ebay.es", "members.ebay.es",
	"mesg.ebay.es", "mesgmy.ebay.es", "my.ebay.es", "myworld.ebay.es",
	"ocs.ebay.es", "ocsnext.ebay.es", "offer.ebay.es", "pages.ebay.es",
	"partnernetwork.ebay.es", "pay.ebay.es", "payments.ebay.es",
	"portal.ebay.es", "postage.ebay.es", "postorder.ebay.es", "pulsar.ebay.es",
	"pulse.ebay.es", "qu.ebay.es", "reg.ebay.es", "res.ebay.es",
	"resolutioncenter.ebay.es", "rover.ebay.es", "scgi.ebay.es",
	"search.ebay.es", "search-completed.ebay.es", "search-desc.ebay.es",
	"sell.ebay.es", "sellerstandards.ebay.es", "shop.ebay.es",
	"signin.ebay.es", "signinalert.ebay.es", "stores.ebay.es", "svcs.ebay.es",
	"viv.ebay.es", "vod.ebay.es", "wantitnow.ebay.es", "wap.ebay.es",
	"www.ebay.es", "ebay.eu", "autodiscover.ebay.eu", "eim.ebay.eu",
	"labs.ebay.eu", "portal.ebay.eu", "rover.ebay.eu", "www.ebay.eu",
	"ebay.fi", "eim.ebay.fi", "fyp.ebay.fi", "ocsnext.ebay.fi",
	"postorder.ebay.fi", "qu.ebay.fi", "rover.ebay.fi", "www.ebay.fi",
	"ebay.fr", "anywhere.ebay.fr", "apibulk.ebay.fr", "applications.ebay.fr",
	"arbd.ebay.fr", "artist-index.ebay.fr", "auth.ebay.fr", "blogs.ebay.fr",
	"bulksell.ebay.fr", "catalog.ebay.fr", "cgi.ebay.fr", "cgi1.ebay.fr",
	"cgi2.ebay.fr", "cgi3.ebay.fr", "cgi4.ebay.fr", "cgi5.ebay.fr",
	"cgi6.ebay.fr", "contact.ebay.fr", "csr.ebay.fr", "epn.ebay.fr",
	"feedback.ebay.fr", "forums.ebay.fr", "fyp.ebay.fr", "hub.ebay.fr",
	"ie8.ebay.fr", "item.ebay.fr", "k2b-bulk.ebay.fr", "listings.ebay.fr",
	"m.ebay.fr", "members.ebay.fr", "mesg.ebay.fr", "mesgmy.ebay.fr",
	"my.ebay.fr", "myworld.ebay.fr", "ocs.ebay.fr", "ocsnext.ebay.fr",
	"offer.ebay.fr", "pages.ebay.fr", "partnernetwork.ebay.fr", "pay.ebay.fr",
	"payments.ebay.fr", "portal.ebay.fr", "postage.ebay.fr",
	"postorder.ebay.fr", "pulsar.ebay.fr", "pulse.ebay.fr", "qu.ebay.fr",
	"reg.ebay.fr", "res.ebay.fr", "resolutioncenter.ebay.fr", "rover.ebay.fr",
	"scgi.ebay.fr", "search-completed.ebay.fr", "sell.ebay.fr",
	"sellerstandards.ebay.fr", "shop.ebay.fr", "signin.ebay.fr",
	"signinalert.ebay.fr", "stores.ebay.fr", "svcs.ebay.fr", "viv.ebay.fr",
	"vod.ebay.fr", "wantitnow.ebay.fr", "wap.ebay.fr", "www.ebay.fr",
	"ebay.gr", "eim.ebay.gr", "fyp.ebay.gr", "ocsnext.ebay.gr",
	"postorder.ebay.gr", "qu.ebay.gr", "rover.ebay.gr", "www.ebay.gr",
	"ebay.hk", "qu.ebay.hk", "ebay.hu", "eim.ebay.hu", "fyp.ebay.hu",
	"ocsnext.ebay.hu", "postorder.ebay.hu", "qu.ebay.hu", "rover.ebay.hu",
	"www.ebay.hu", "ebay.ie", "affiliates.ebay.ie", "anywhere.ebay.ie",
	"apibulk.ebay.ie", "applications.ebay.ie", "arbd.ebay.ie",
	"artist-index.ebay.ie", "auth.ebay.ie", "blogs.ebay.ie",
	"bulksell.ebay.ie", "catalog.ebay.ie", "cgi.ebay.ie", "cgi1.ebay.ie",
	"cgi2.ebay.ie", "cgi3.ebay.ie", "cgi4.ebay.ie", "cgi5.ebay.ie",
	"cgi6.ebay.ie", "coins.ebay.ie", "contact.ebay.ie", "csr.ebay.ie",
	"epn.ebay.ie", "feedback.ebay.ie", "fyp.ebay.ie", "hub.ebay.ie",
	"item.ebay.ie", "k2b-bulk.ebay.ie", "listings.ebay.ie", "m.ebay.ie",
	"members.ebay.ie", "mesg.ebay.ie", "mesgmy.ebay.ie", "motors.ebay.ie",
	"my.ebay.ie", "myworld.ebay.ie", "ocs.ebay.ie", "ocsnext.ebay.ie",
	"offer.ebay.ie", "pages.ebay.ie", "pay.ebay.ie", "payments.ebay.ie",
	"portal.ebay.ie", "postorder.ebay.ie", "pulsar.ebay.ie", "pulse.ebay.ie",
	"qu.ebay.ie", "reg.ebay.ie", "res.ebay.ie", "rover.ebay.ie",
	"scgi.ebay.ie", "search-completed.ebay.ie", "sell.ebay.ie",
	"sellerstandards.ebay.ie", "shop.ebay.ie", "signin.ebay.ie",
	"signinalert.ebay.ie", "stores.ebay.ie", "svcs.ebay.ie", "viv.ebay.ie",
	"wantitnow.ebay.ie", "wap.ebay.ie", "www.ebay.ie", "ebay.in",
	"affiliates.ebay.in", "anywhere.ebay.in", "apibulk.ebay.in",
	"arbd.ebay.in", "artist.ebay.in", "artist-index.ebay.in", "auth.ebay.in",
	"blogs.ebay.in", "bulksell.ebay.in", "catalog.ebay.in", "cgi.ebay.in",
	"cgi1.ebay.in", "cgi2.ebay.in", "cgi3.ebay.in", "cgi4.ebay.in",
	"cgi5.ebay.in", "cgi6.ebay.in", "community.ebay.in", "contact.ebay.in",
	"csr.ebay.in", "deals.ebay.in", "epn.ebay.in", "feedback.ebay.in",
	"forums.ebay.in", "fyp.ebay.in", "geb.ebay.in", "hub.ebay.in",
	"item.ebay.in", "k2b-bulk.ebay.in", "listings.ebay.in", "m.ebay.in",
	"members.ebay.in", "mesg.ebay.in", "mesgmy.ebay.in", "my.ebay.in",
	"myworld.ebay.in", "ocs.ebay.in", "ocsnext.ebay.in", "offer.ebay.in",
	"pages.ebay.in", "pay.ebay.in", "payments.ebay.in", "popular.ebay.in",
	"postorder.ebay.in", "product.ebay.in", "pulsar.ebay.in", "pulse.ebay.in",
	"qu.ebay.in", "reg.ebay.in", "res.ebay.in", "reviews.ebay.in",
	"rover.ebay.in", "scgi.ebay.in", "search-completed.ebay.in",
	"search-desc.ebay.in", "sell.ebay.in", "sellerstandards.ebay.in",
	"shop.ebay.in", "signin.ebay.in", "signinalert.ebay.in", "stores.ebay.in",
	"svcs.ebay.in", "viv.ebay.in", "wantitnow.ebay.in", "www.ebay.in",
	"wwww.ebay.in", "ebay.it", "affiliates.ebay.it", "anywhere.ebay.it",
	"apibulk.ebay.it", "applications.ebay.it", "arbd.ebay.it",
	"artist-index.ebay.it", "auth.ebay.it", "blogs.ebay.it",
	"bulksell.ebay.it", "catalog.ebay.it", "cgi.ebay.it", "cgi1.ebay.it",
	"cgi2.ebay.it", "cgi3.ebay.it", "cgi4.ebay.it", "cgi5.ebay.it",
	"cgi6.ebay.it", "community.ebay.it", "compare.ebay.it", "contact.ebay.it",
	"csr.ebay.it", "epn.ebay.it", "feedback.ebay.it", "forums.ebay.it",
	"fyp.ebay.it", "hub.ebay.it", "item.ebay.it", "k2b-bulk.ebay.it",
	"listings.ebay.it", "m.ebay.it", "members.ebay.it", "mesg.ebay.it",
	"mesgmy.ebay.it", "mobile.ebay.it", "my.ebay.it", "myworld.ebay.it",
	"ocs.ebay.it", "ocsnext.ebay.it", "offer.ebay.it", "pages.ebay.it",
	"partnernetwork.ebay.it", "pay.ebay.it", "payments.ebay.it",
	"portal.ebay.it", "postage.ebay.it", "postorder.ebay.it", "pulsar.ebay.it",
	"pulse.ebay.it", "qu.ebay.it", "reg.ebay.it", "res.ebay.it",
	"resolutioncenter.ebay.it", "rover.ebay.it", "scgi.ebay.it",
	"search.ebay.it", "search-completed.ebay.it", "sell.ebay.it",
	"sellerstandards.ebay.it", "shop.ebay.it", "signin.ebay.it",
	"signinalert.ebay.it", "stores.ebay.it", "svcs.ebay.it", "viv.ebay.it",
	"vod.ebay.it", "wantitnow.ebay.it", "wap.ebay.it", "www.ebay.it",
	"wwww.ebay.it", "ebay.mx", "www.ebay.mx", "ebay.my",
	"adjustdiscount.ebay.my", "adn.ebay.my", "ads.ebay.my",
	"affiliates.ebay.my", "aka-msa-b1.ebay.my", "announcements.ebay.my",
	"anywhere.ebay.my", "apacshippingtool.ebay.my", "apibulk.ebay.my",
	"apics1.ebay.my", "apiweb.ebay.my", "app.ebay.my", "applications.ebay.my",
	"apps.ebay.my", "arbd.ebay.my", "artist.ebay.my", "artist-index.ebay.my",
	"att.ebay.my", "attr-search.ebay.my", "auth.ebay.my",
	"autocomplete.ebay.my", "autodiscover.ebay.my", "bestof.ebay.my",
	"blog.ebay.my", "blogs.ebay.my", "brands.ebay.my", "bulksell.ebay.my",
	"catalog.ebay.my", "cc.ebay.my", "celebrity.ebay.my",
	"certifiedprovider.ebay.my", "cgi.ebay.my", "cgi1.ebay.my", "cgi2.ebay.my",
	"cgi3.ebay.my", "cgi4.ebay.my", "cgi5.ebay.my", "cgi6.ebay.my",
	"charity.ebay.my", "coins.ebay.my", "community.ebay.my", "compare.ebay.my",
	"contact.ebay.my", "countdown.ebay.my", "csr.ebay.my", "deals.ebay.my",
	"desktop.ebay.my", "developer.ebay.my", "developer.ebay.my",
	"donations.ebay.my", "donationsstatic.ebay.my", "eim.ebay.my",
	"epn.ebay.my", "es.ebay.my", "esp.ebay.my", "events.ebay.my",
	"feedback.ebay.my", "forums.ebay.my", "fyp.ebay.my", "garden.ebay.my",
	"geb.ebay.my", "giftcard.ebay.my", "givingworks.ebay.my", "global.ebay.my",
	"globaldeals.ebay.my", "green.ebay.my", "groupgifts.ebay.my",
	"groups.ebay.my", "haitao.ebay.my", "half.ebay.my", "hub.ebay.my",
	"id.ebay.my", "ie8.ebay.my", "instantsale.ebay.my", "instantsale.ebay.my",
	"investor.ebay.my", "item.ebay.my", "k2b-bulk.ebay.my", "labs.ebay.my",
	"listing-index.ebay.my", "listings.ebay.my", "live.ebay.my",
	"liveauctions.ebay.my", "m.ebay.my", "members.ebay.my", "mesg.ebay.my",
	"mesgmy.ebay.my", "mmm.ebay.my", "mobile.ebay.my", "morelikethis.ebay.my",
	"motors.ebay.my", "my.ebay.my", "myworld.ebay.my", "neighborhoods.ebay.my",
	"news.ebay.my", "nikemag.ebay.my", "now.ebay.my", "ocs.ebay.my",
	"ocsnext.ebay.my", "offer.ebay.my", "pages.ebay.my",
	"partnernetwork.ebay.my", "pay.ebay.my", "payments.ebay.my",
	"play.ebay.my", "popular.ebay.my", "portal.ebay.my", "postage.ebay.my",
	"postorder.ebay.my", "product.ebay.my", "pulsar.ebay.my", "pulse.ebay.my",
	"qu.ebay.my", "r.ebay.my", "recommendations.ebay.my", "reg.ebay.my",
	"res.ebay.my", "resolutioncenter.ebay.my", "reviews.ebay.my",
	"rover.ebay.my", "sandbox.ebay.my", "scgi.ebay.my", "sea.ebay.my",
	"search.ebay.my", "search-completed.ebay.my", "search-desc.ebay.my",
	"securethumbs.ebay.my", "sell.ebay.my", "sellerstandards.ebay.my",
	"sellforme.ebay.my", "sellitforward.ebay.my", "services.ebay.my",
	"shop.ebay.my", "signin.ebay.my", "signinalert.ebay.my", "store.ebay.my",
	"stores.ebay.my", "stuff.ebay.my", "stylestories.ebay.my",
	"survey.ebay.my", "svcs.ebay.my", "tahiti.ebay.my", "tech.ebay.my",
	"transformers.ebay.my", "twexport.ebay.my", "viv.ebay.my", "vod.ebay.my",
	"wantitnow.ebay.my", "wap.ebay.my", "worldofgood.ebay.my", "www.ebay.my",
	"www2.ebay.my", "wwww.ebay.my", "ebay.nl", "affiliates.ebay.nl",
	"anywhere.ebay.nl", "apibulk.ebay.nl", "applications.ebay.nl",
	"arbd.ebay.nl", "artist-index.ebay.nl", "auth.ebay.nl", "blogs.ebay.nl",
	"bulksell.ebay.nl", "catalog.ebay.nl", "cgi.ebay.nl", "cgi1.ebay.nl",
	"cgi2.ebay.nl", "cgi3.ebay.nl", "cgi4.ebay.nl", "cgi5.ebay.nl",
	"cgi6.ebay.nl", "community.ebay.nl", "contact.ebay.nl", "csr.ebay.nl",
	"epn.ebay.nl", "feedback.ebay.nl", "fyp.ebay.nl", "hub.ebay.nl",
	"item.ebay.nl", "k2b-bulk.ebay.nl", "listings.ebay.nl", "m.ebay.nl",
	"members.ebay.nl", "mesg.ebay.nl", "mesgmy.ebay.nl", "motors.ebay.nl",
	"my.ebay.nl", "myworld.ebay.nl", "ocs.ebay.nl", "ocsnext.ebay.nl",
	"offer.ebay.nl", "pages.ebay.nl", "pay.ebay.nl", "payments.ebay.nl",
	"portal.ebay.nl", "postorder.ebay.nl", "pulsar.ebay.nl", "pulse.ebay.nl",
	"qu.ebay.nl", "reg.ebay.nl", "res.ebay.nl", "resolutioncenter.ebay.nl",
	"rover.ebay.nl", "scgi.ebay.nl", "search-completed.ebay.nl",
	"sell.ebay.nl", "sellerstandards.ebay.nl", "shop.ebay.nl",
	"signin.ebay.nl", "signinalert.ebay.nl", "stores.ebay.nl", "svcs.ebay.nl",
	"viv.ebay.nl", "wantitnow.ebay.nl", "www.ebay.nl", "ebay.no",
	"eim.ebay.no", "fyp.ebay.no", "ocsnext.ebay.no", "postorder.ebay.no",
	"qu.ebay.no", "rover.ebay.no", "www.ebay.no", "ebay.no", "eim.ebay.no",
	"fyp.ebay.no", "ocsnext.ebay.no", "postorder.ebay.no", "qu.ebay.no",
	"rover.ebay.no", "www.ebay.no", "ebay.ph", "affiliates.ebay.ph",
	"anywhere.ebay.ph", "apibulk.ebay.ph", "applications.ebay.ph",
	"arbd.ebay.ph", "artist-index.ebay.ph", "auth.ebay.ph", "blogs.ebay.ph",
	"bulksell.ebay.ph", "catalog.ebay.ph", "cgi.ebay.ph", "cgi1.ebay.ph",
	"cgi2.ebay.ph", "cgi3.ebay.ph", "cgi4.ebay.ph", "cgi5.ebay.ph",
	"cgi6.ebay.ph", "contact.ebay.ph", "csr.ebay.ph", "deals.ebay.ph",
	"developer.ebay.ph", "developer.ebay.ph", "epn.ebay.ph",
	"feedback.ebay.ph", "fyp.ebay.ph", "hub.ebay.ph", "item.ebay.ph",
	"k2b-bulk.ebay.ph", "listings.ebay.ph", "m.ebay.ph", "members.ebay.ph",
	"mesg.ebay.ph", "mesgmy.ebay.ph", "mmm.ebay.ph", "my.ebay.ph",
	"myworld.ebay.ph", "ocs.ebay.ph", "ocsnext.ebay.ph", "offer.ebay.ph",
	"pages.ebay.ph", "pay.ebay.ph", "payments.ebay.ph", "popular.ebay.ph",
	"portal.ebay.ph", "postorder.ebay.ph", "pulsar.ebay.ph", "qu.ebay.ph",
	"reg.ebay.ph", "res.ebay.ph", "resolutioncenter.ebay.ph", "rover.ebay.ph",
	"scgi.ebay.ph", "search-completed.ebay.ph", "sell.ebay.ph",
	"sellerstandards.ebay.ph", "shop.ebay.ph", "signin.ebay.ph",
	"signinalert.ebay.ph", "stores.ebay.ph", "svcs.ebay.ph", "viv.ebay.ph",
	"wantitnow.ebay.ph", "wap.ebay.ph", "www.ebay.ph", "ebay.pl",
	"affiliates.ebay.pl", "anywhere.ebay.pl", "apibulk.ebay.pl",
	"applications.ebay.pl", "arbd.ebay.pl", "artist-index.ebay.pl",
	"auth.ebay.pl", "blogs.ebay.pl", "bulksell.ebay.pl", "catalog.ebay.pl",
	"cgi.ebay.pl", "cgi1.ebay.pl", "cgi2.ebay.pl", "cgi3.ebay.pl",
	"cgi4.ebay.pl", "cgi5.ebay.pl", "cgi6.ebay.pl", "community.ebay.pl",
	"contact.ebay.pl", "csr.ebay.pl", "epn.ebay.pl", "feedback.ebay.pl",
	"fyp.ebay.pl", "hub.ebay.pl", "item.ebay.pl", "k2b-bulk.ebay.pl",
	"listings.ebay.pl", "m.ebay.pl", "members.ebay.pl", "mesg.ebay.pl",
	"mesgmy.ebay.pl", "mmm.ebay.pl", "motors.ebay.pl", "my.ebay.pl",
	"myworld.ebay.pl", "ocs.ebay.pl", "ocsnext.ebay.pl", "offer.ebay.pl",
	"pages.ebay.pl", "pay.ebay.pl", "payments.ebay.pl", "popular.ebay.pl",
	"postorder.ebay.pl", "pulsar.ebay.pl", "pulse.ebay.pl", "qu.ebay.pl",
	"reg.ebay.pl", "res.ebay.pl", "resolutioncenter.ebay.pl", "rover.ebay.pl",
	"scgi.ebay.pl", "search-completed.ebay.pl", "sell.ebay.pl",
	"sellerstandards.ebay.pl", "shop.ebay.pl", "signin.ebay.pl",
	"signinalert.ebay.pl", "stores.ebay.pl", "svcs.ebay.pl", "viv.ebay.pl",
	"wantitnow.ebay.pl", "www.ebay.pl", "ebay.pt", "eim.ebay.pt",
	"fyp.ebay.pt", "ocsnext.ebay.pt", "postorder.ebay.pt", "qu.ebay.pt",
	"rover.ebay.pt", "www.ebay.pt", "ebay.pt", "eim.ebay.pt", "fyp.ebay.pt",
	"ocsnext.ebay.pt", "postorder.ebay.pt", "qu.ebay.pt", "rover.ebay.pt",
	"www.ebay.pt", "ebay.ru", "auth.ebay.ru", "eim.ebay.ru", "fyp.ebay.ru",
	"mesg.ebay.ru", "ocsnext.ebay.ru", "pay.ebay.ru", "postorder.ebay.ru",
	"qu.ebay.ru", "rover.ebay.ru", "signin.ebay.ru", "www.ebay.ru", "ebay.se",
	"eim.ebay.se", "pages.ebay.se", "postorder.ebay.se", "qu.ebay.se",
	"rover.ebay.se", "www.ebay.se", "ebay.sg", "adjustdiscount.ebay.sg",
	"adn.ebay.sg", "ads.ebay.sg", "affiliates.ebay.sg", "aka-msa-b1.ebay.sg",
	"announcements.ebay.sg", "anywhere.ebay.sg", "apacshippingtool.ebay.sg",
	"apibulk.ebay.sg", "apics1.ebay.sg", "apiweb.ebay.sg", "app.ebay.sg",
	"applications.ebay.sg", "apps.ebay.sg", "arbd.ebay.sg", "artist.ebay.sg",
	"artist-index.ebay.sg", "att.ebay.sg", "attr-search.ebay.sg",
	"auth.ebay.sg", "autocomplete.ebay.sg", "autodiscover.ebay.sg",
	"bestof.ebay.sg", "blog.ebay.sg", "blogs.ebay.sg", "brands.ebay.sg",
	"bulksell.ebay.sg", "catalog.ebay.sg", "cc.ebay.sg", "celebrity.ebay.sg",
	"certifiedprovider.ebay.sg", "cgi.ebay.sg", "cgi1.ebay.sg", "cgi2.ebay.sg",
	"cgi3.ebay.sg", "cgi4.ebay.sg", "cgi5.ebay.sg", "cgi6.ebay.sg",
	"charity.ebay.sg", "coins.ebay.sg", "community.ebay.sg", "compare.ebay.sg",
	"contact.ebay.sg", "countdown.ebay.sg", "csr.ebay.sg", "deals.ebay.sg",
	"desktop.ebay.sg", "developer.ebay.sg", "developer.ebay.sg",
	"donations.ebay.sg", "donationsstatic.ebay.sg", "eim.ebay.sg",
	"epn.ebay.sg", "es.ebay.sg", "esp.ebay.sg", "events.ebay.sg",
	"feedback.ebay.sg", "forums.ebay.sg", "fyp.ebay.sg", "garden.ebay.sg",
	"geb.ebay.sg", "giftcard.ebay.sg", "givingworks.ebay.sg", "global.ebay.sg",
	"globaldeals.ebay.sg", "green.ebay.sg", "groupgifts.ebay.sg",
	"groups.ebay.sg", "haitao.ebay.sg", "half.ebay.sg", "hub.ebay.sg",
	"id.ebay.sg", "ie8.ebay.sg", "instantsale.ebay.sg", "instantsale.ebay.sg",
	"investor.ebay.sg", "item.ebay.sg", "k2b-bulk.ebay.sg", "labs.ebay.sg",
	"listing-index.ebay.sg", "listings.ebay.sg", "live.ebay.sg",
	"liveauctions.ebay.sg", "m.ebay.sg", "members.ebay.sg", "mesg.ebay.sg",
	"mesgmy.ebay.sg", "mmm.ebay.sg", "mobile.ebay.sg", "morelikethis.ebay.sg",
	"motors.ebay.sg", "my.ebay.sg", "myworld.ebay.sg", "neighborhoods.ebay.sg",
	"news.ebay.sg", "nikemag.ebay.sg", "now.ebay.sg", "ocs.ebay.sg",
	"ocsnext.ebay.sg", "offer.ebay.sg", "pages.ebay.sg",
	"partnernetwork.ebay.sg", "pay.ebay.sg", "payments.ebay.sg",
	"play.ebay.sg", "popular.ebay.sg", "portal.ebay.sg", "postage.ebay.sg",
	"postorder.ebay.sg", "product.ebay.sg", "pulsar.ebay.sg", "pulse.ebay.sg",
	"qu.ebay.sg", "r.ebay.sg", "recommendations.ebay.sg", "reg.ebay.sg",
	"res.ebay.sg", "resolutioncenter.ebay.sg", "reviews.ebay.sg",
	"rover.ebay.sg", "sandbox.ebay.sg", "scgi.ebay.sg", "sea.ebay.sg",
	"search.ebay.sg", "search-completed.ebay.sg", "search-desc.ebay.sg",
	"securethumbs.ebay.sg", "sell.ebay.sg", "sellerstandards.ebay.sg",
	"sellforme.ebay.sg", "sellitforward.ebay.sg", "services.ebay.sg",
	"shop.ebay.sg", "signin.ebay.sg", "signinalert.ebay.sg", "store.ebay.sg",
	"stores.ebay.sg", "stuff.ebay.sg", "stylestories.ebay.sg",
	"survey.ebay.sg", "svcs.ebay.sg", "tahiti.ebay.sg", "tech.ebay.sg",
	"transformers.ebay.sg", "twexport.ebay.sg", "viv.ebay.sg", "vod.ebay.sg",
	"wantitnow.ebay.sg", "wap.ebay.sg", "worldofgood.ebay.sg", "www.ebay.sg",
	"www2.ebay.sg", "wwww.ebay.sg", "ebay.th", "adjustdiscount.ebay.th",
	"adn.ebay.th", "ads.ebay.th", "affiliates.ebay.th", "aka-msa-b1.ebay.th",
	"announcements.ebay.th", "anywhere.ebay.th", "apacshippingtool.ebay.th",
	"apibulk.ebay.th", "apics1.ebay.th", "apiweb.ebay.th", "app.ebay.th",
	"applications.ebay.th", "apps.ebay.th", "arbd.ebay.th", "artist.ebay.th",
	"artist-index.ebay.th", "att.ebay.th", "attr-search.ebay.th",
	"auth.ebay.th", "autocomplete.ebay.th", "autodiscover.ebay.th",
	"bestof.ebay.th", "blog.ebay.th", "blogs.ebay.th", "brands.ebay.th",
	"bulksell.ebay.th", "catalog.ebay.th", "cc.ebay.th", "celebrity.ebay.th",
	"certifiedprovider.ebay.th", "cgi.ebay.th", "cgi1.ebay.th", "cgi2.ebay.th",
	"cgi3.ebay.th", "cgi4.ebay.th", "cgi5.ebay.th", "cgi6.ebay.th",
	"charity.ebay.th", "coins.ebay.th", "community.ebay.th", "compare.ebay.th",
	"contact.ebay.th", "countdown.ebay.th", "csr.ebay.th", "deals.ebay.th",
	"desktop.ebay.th", "developer.ebay.th", "developer.ebay.th",
	"donations.ebay.th", "donationsstatic.ebay.th", "eim.ebay.th",
	"epn.ebay.th", "es.ebay.th", "esp.ebay.th", "events.ebay.th",
	"feedback.ebay.th", "forums.ebay.th", "fyp.ebay.th", "garden.ebay.th",
	"geb.ebay.th", "giftcard.ebay.th", "givingworks.ebay.th", "global.ebay.th",
	"globaldeals.ebay.th", "green.ebay.th", "groupgifts.ebay.th",
	"groups.ebay.th", "haitao.ebay.th", "half.ebay.th", "hub.ebay.th",
	"id.ebay.th", "ie8.ebay.th", "instantsale.ebay.th", "instantsale.ebay.th",
	"investor.ebay.th", "item.ebay.th", "k2b-bulk.ebay.th", "labs.ebay.th",
	"listing-index.ebay.th", "listings.ebay.th", "live.ebay.th",
	"liveauctions.ebay.th", "m.ebay.th", "members.ebay.th", "mesg.ebay.th",
	"mesgmy.ebay.th", "mmm.ebay.th", "mobile.ebay.th", "morelikethis.ebay.th",
	"motors.ebay.th", "my.ebay.th", "myworld.ebay.th", "neighborhoods.ebay.th",
	"news.ebay.th", "nikemag.ebay.th", "now.ebay.th", "ocs.ebay.th",
	"ocsnext.ebay.th", "offer.ebay.th", "pages.ebay.th",
	"partnernetwork.ebay.th", "pay.ebay.th", "payments.ebay.th",
	"play.ebay.th", "popular.ebay.th", "portal.ebay.th", "postage.ebay.th",
	"postorder.ebay.th", "product.ebay.th", "pulsar.ebay.th", "pulse.ebay.th",
	"qu.ebay.th", "r.ebay.th", "recommendations.ebay.th", "reg.ebay.th",
	"res.ebay.th", "resolutioncenter.ebay.th", "reviews.ebay.th",
	"rover.ebay.th", "sandbox.ebay.th", "scgi.ebay.th", "sea.ebay.th",
	"search.ebay.th", "search-completed.ebay.th", "search-desc.ebay.th",
	"securethumbs.ebay.th", "sell.ebay.th", "sellerstandards.ebay.th",
	"sellforme.ebay.th", "sellitforward.ebay.th", "services.ebay.th",
	"shop.ebay.th", "signin.ebay.th", "signinalert.ebay.th", "store.ebay.th",
	"stores.ebay.th", "stuff.ebay.th", "stylestories.ebay.th",
	"survey.ebay.th", "svcs.ebay.th", "tahiti.ebay.th", "tech.ebay.th",
	"transformers.ebay.th", "twexport.ebay.th", "viv.ebay.th", "vod.ebay.th",
	"wantitnow.ebay.th", "wap.ebay.th", "worldofgood.ebay.th", "www.ebay.th",
	"www2.ebay.th", "wwww.ebay.th", "ebay.tw", "adjustdiscount.ebay.tw",
	"adn.ebay.tw", "ads.ebay.tw", "affiliates.ebay.tw", "aka-msa-b1.ebay.tw",
	"announcements.ebay.tw", "anywhere.ebay.tw", "apacshippingtool.ebay.tw",
	"apibulk.ebay.tw", "apics1.ebay.tw", "apiweb.ebay.tw", "app.ebay.tw",
	"applications.ebay.tw", "apps.ebay.tw", "arbd.ebay.tw", "artist.ebay.tw",
	"artist-index.ebay.tw", "att.ebay.tw", "attr-search.ebay.tw",
	"auth.ebay.tw", "autocomplete.ebay.tw", "autodiscover.ebay.tw",
	"bestof.ebay.tw", "blog.ebay.tw", "blogs.ebay.tw", "brands.ebay.tw",
	"bulksell.ebay.tw", "catalog.ebay.tw", "cc.ebay.tw", "celebrity.ebay.tw",
	"certifiedprovider.ebay.tw", "cgi.ebay.tw", "cgi1.ebay.tw", "cgi2.ebay.tw",
	"cgi3.ebay.tw", "cgi4.ebay.tw", "cgi5.ebay.tw", "cgi6.ebay.tw",
	"charity.ebay.tw", "coins.ebay.tw", "community.ebay.tw", "compare.ebay.tw",
	"contact.ebay.tw", "countdown.ebay.tw", "csr.ebay.tw", "deals.ebay.tw",
	"desktop.ebay.tw", "developer.ebay.tw", "developer.ebay.tw",
	"donations.ebay.tw", "donationsstatic.ebay.tw", "eim.ebay.tw",
	"epn.ebay.tw", "es.ebay.tw", "esp.ebay.tw", "events.ebay.tw",
	"feedback.ebay.tw", "forums.ebay.tw", "fyp.ebay.tw", "garden.ebay.tw",
	"geb.ebay.tw", "giftcard.ebay.tw", "givingworks.ebay.tw", "global.ebay.tw",
	"globaldeals.ebay.tw", "green.ebay.tw", "groupgifts.ebay.tw",
	"groups.ebay.tw", "haitao.ebay.tw", "half.ebay.tw", "hub.ebay.tw",
	"id.ebay.tw", "ie8.ebay.tw", "instantsale.ebay.tw", "instantsale.ebay.tw",
	"investor.ebay.tw", "item.ebay.tw", "k2b-bulk.ebay.tw", "labs.ebay.tw",
	"listing-index.ebay.tw", "listings.ebay.tw", "live.ebay.tw",
	"liveauctions.ebay.tw", "m.ebay.tw", "members.ebay.tw", "mesg.ebay.tw",
	"mesgmy.ebay.tw", "mmm.ebay.tw", "mobile.ebay.tw", "morelikethis.ebay.tw",
	"motors.ebay.tw", "my.ebay.tw", "myworld.ebay.tw", "neighborhoods.ebay.tw",
	"news.ebay.tw", "nikemag.ebay.tw", "now.ebay.tw", "ocs.ebay.tw",
	"ocsnext.ebay.tw", "offer.ebay.tw", "pages.ebay.tw",
	"partnernetwork.ebay.tw", "pay.ebay.tw", "payments.ebay.tw",
	"play.ebay.tw", "popular.ebay.tw", "portal.ebay.tw", "postage.ebay.tw",
	"postorder.ebay.tw", "product.ebay.tw", "pulsar.ebay.tw", "pulse.ebay.tw",
	"qu.ebay.tw", "r.ebay.tw", "recommendations.ebay.tw", "reg.ebay.tw",
	"res.ebay.tw", "resolutioncenter.ebay.tw", "reviews.ebay.tw",
	"rover.ebay.tw", "sandbox.ebay.tw", "scgi.ebay.tw", "sea.ebay.tw",
	"search.ebay.tw", "search-completed.ebay.tw", "search-desc.ebay.tw",
	"securethumbs.ebay.tw", "sell.ebay.tw", "sellerstandards.ebay.tw",
	"sellforme.ebay.tw", "sellitforward.ebay.tw", "services.ebay.tw",
	"shop.ebay.tw", "signin.ebay.tw", "signinalert.ebay.tw", "store.ebay.tw",
	"stores.ebay.tw", "stuff.ebay.tw", "stylestories.ebay.tw",
	"survey.ebay.tw", "svcs.ebay.tw", "tahiti.ebay.tw", "tech.ebay.tw",
	"transformers.ebay.tw", "twexport.ebay.tw", "viv.ebay.tw", "vod.ebay.tw",
	"wantitnow.ebay.tw", "wap.ebay.tw", "worldofgood.ebay.tw", "www.ebay.tw",
	"www2.ebay.tw", "wwww.ebay.tw", "ebay.uk", "ebay.us", "ebaycbt.co.kr",
	"www.ebaycbt.co.kr"
];

const MAC_ADDON_ID = "@testpilot-containers";

let macAddonEnabled = false;
let ebayCookieStoreId = null;

const canceledRequests = {};
const tabsWaitingToLoad = {};
const ebayHostREs = [];

async function isMACAddonEnabled () {
  try {
    const macAddonInfo = await browser.management.get(MAC_ADDON_ID);
    if (macAddonInfo.enabled) {
      sendJailedDomainsToMAC();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

async function setupMACAddonListeners () {
  browser.runtime.onMessageExternal.addListener((message, sender) => {
    if (sender.id !== "@testpilot-containers") {
      return;
    }
    switch (message.method) {
    case "MACListening":
      sendJailedDomainsToMAC();
      break;
    }
  });
  function disabledExtension (info) {
    if (info.id === MAC_ADDON_ID) {
      macAddonEnabled = false;
    }
  }
  function enabledExtension (info) {
    if (info.id === MAC_ADDON_ID) {
      macAddonEnabled = true;
    }
  }
  browser.management.onInstalled.addListener(enabledExtension);
  browser.management.onEnabled.addListener(enabledExtension);
  browser.management.onUninstalled.addListener(disabledExtension);
  browser.management.onDisabled.addListener(disabledExtension);
}

async function sendJailedDomainsToMAC () {
  try {
    return await browser.runtime.sendMessage(MAC_ADDON_ID, {
      method: "jailedDomains",
      urls: EBAY_DOMAINS.map((domain) => {
        return `https://${domain}/`;
      })
    });
  } catch (e) {
    // We likely might want to handle this case: https://github.com/mozilla/contain-facebook/issues/113#issuecomment-380444165
    return false;
  }
}

async function getMACAssignment (url) {
  if (!macAddonEnabled) {
    return false;
  }

  try {
    const assignment = await browser.runtime.sendMessage(MAC_ADDON_ID, {
      method: "getAssignment",
      url
    });
    return assignment;
  } catch (e) {
    return false;
  }
}

function cancelRequest (tab, options) {
  // we decided to cancel the request at this point, register canceled request
  canceledRequests[tab.id] = {
    requestIds: {
      [options.requestId]: true
    },
    urls: {
      [options.url]: true
    }
  };

  // since webRequest onCompleted and onErrorOccurred are not 100% reliable
  // we register a timer here to cleanup canceled requests, just to make sure we don't
  // end up in a situation where certain urls in a tab.id stay canceled
  setTimeout(() => {
    if (canceledRequests[tab.id]) {
      delete canceledRequests[tab.id];
    }
  }, 2000);
}

function shouldCancelEarly (tab, options) {
  // we decided to cancel the request at this point
  if (!canceledRequests[tab.id]) {
    cancelRequest(tab, options);
  } else {
    let cancelEarly = false;
    if (canceledRequests[tab.id].requestIds[options.requestId] ||
        canceledRequests[tab.id].urls[options.url]) {
      // same requestId or url from the same tab
      // this is a redirect that we have to cancel early to prevent opening two tabs
      cancelEarly = true;
    }
    // register this requestId and url as canceled too
    canceledRequests[tab.id].requestIds[options.requestId] = true;
    canceledRequests[tab.id].urls[options.url] = true;
    if (cancelEarly) {
      return true;
    }
  }
  return false;
}

function generateEbayHostREs () {
  for (let ebayDomain of EBAY_DOMAINS) {
    ebayHostREs.push(new RegExp(`^(.*\\.)?${ebayDomain}$`));
  }
}

async function clearEbayCookies () {
  // Clear all ebay cookies
  const containers = await browser.contextualIdentities.query({});
  containers.push({
    cookieStoreId: "firefox-default"
  });

  let macAssignments = [];
  if (macAddonEnabled) {
    const promises = EBAY_DOMAINS.map(async ebayDomain => {
      const assigned = await getMACAssignment(`https://${ebayDomain}/`);
      return assigned ? ebayDomain : null;
    });
    macAssignments = await Promise.all(promises);
  }

  EBAY_DOMAINS.map(async ebayDomain => {
    const ebayCookieUrl = `https://${ebayDomain}/`;

    // dont clear cookies for ebayDomain if mac assigned (with or without www.)
    if (macAddonEnabled &&
        (macAssignments.includes(ebayDomain) ||
         macAssignments.includes(`www.${ebayDomain}`))) {
      return;
    }

    containers.map(async container => {
      const storeId = container.cookieStoreId;
      if (storeId === ebayCookieStoreId) {
        // Don't clear cookies in the Ebay Container
        return;
      }

      const cookies = await browser.cookies.getAll({
        domain: ebayDomain,
        storeId
      });

      cookies.map(cookie => {
        browser.cookies.remove({
          name: cookie.name,
          url: ebayCookieUrl,
          storeId
        });
      });
      // Also clear Service Workers as it breaks detecting onBeforeRequest
      await browser.browsingData.remove({hostnames: [ebayDomain]}, {serviceWorkers: true});
    });
  });
}

async function setupContainer () {
  // Use existing Ebay container, or create one
  const contexts = await browser.contextualIdentities.query({name: EBAY_CONTAINER_NAME});
  if (contexts.length > 0) {
    ebayCookieStoreId = contexts[0].cookieStoreId;
  } else {
    const context = await browser.contextualIdentities.create({
      name: EBAY_CONTAINER_NAME,
      color: EBAY_CONTAINER_COLOR,
      icon: EBAY_CONTAINER_ICON
    });
    ebayCookieStoreId = context.cookieStoreId;
  }
}

function reopenTab ({url, tab, cookieStoreId}) {
  browser.tabs.create({
    url,
    cookieStoreId,
    active: tab.active,
    index: tab.index,
    windowId: tab.windowId
  });
  browser.tabs.remove(tab.id);
}

function isEbayURL (url) {
  const parsedUrl = new URL(url);
  for (let ebayHostRE of ebayHostREs) {
    if (ebayHostRE.test(parsedUrl.host)) {
      return true;
    }
  }
  return false;
}

function shouldContainInto (url, tab) {
  if (!url.startsWith("http")) {
    // we only handle URLs starting with http(s)
    return false;
  }

  if (isEbayURL(url)) {
    if (tab.cookieStoreId !== ebayCookieStoreId) {
      // Ebay-URL outside of Ebay Container Tab
      // Should contain into Ebay Container
      return ebayCookieStoreId;
    }
  } else if (tab.cookieStoreId === ebayCookieStoreId) {
    // Non-Ebay-URL inside Ebay Container Tab
    // Should contain into Default Container
    return "firefox-default";
  }

  return false;
}

async function maybeReopenAlreadyOpenTabs () {
  const maybeReopenTab = async tab => {
    const macAssigned = await getMACAssignment(tab.url);
    if (macAssigned) {
      // We don't reopen MAC assigned urls
      return;
    }
    const cookieStoreId = shouldContainInto(tab.url, tab);
    if (!cookieStoreId) {
      // Tab doesn't need to be contained
      return;
    }
    reopenTab({
      url: tab.url,
      tab,
      cookieStoreId
    });
  };

  const tabsOnUpdated = (tabId, changeInfo, tab) => {
    if (changeInfo.url && tabsWaitingToLoad[tabId]) {
      // Tab we're waiting for switched it's url, maybe we reopen
      delete tabsWaitingToLoad[tabId];
      maybeReopenTab(tab);
    }
    if (tab.status === "complete" && tabsWaitingToLoad[tabId]) {
      // Tab we're waiting for completed loading
      delete tabsWaitingToLoad[tabId];
    }
    if (!Object.keys(tabsWaitingToLoad).length) {
      // We're done waiting for tabs to load, remove event listener
      browser.tabs.onUpdated.removeListener(tabsOnUpdated);
    }
  };

  // Query for already open Tabs
  const tabs = await browser.tabs.query({});
  tabs.map(async tab => {
    if (tab.incognito) {
      return;
    }
    if (tab.url === "about:blank") {
      if (tab.status !== "loading") {
        return;
      }
      // about:blank Tab is still loading, so we indicate that we wait for it to load
      // and register the event listener if we haven't yet.
      //
      // This is a workaround until platform support is implemented:
      // https://bugzilla.mozilla.org/show_bug.cgi?id=1447551
      // https://github.com/mozilla/multi-account-containers/issues/474
      tabsWaitingToLoad[tab.id] = true;
      if (!browser.tabs.onUpdated.hasListener(tabsOnUpdated)) {
        browser.tabs.onUpdated.addListener(tabsOnUpdated);
      }
    } else {
      // Tab already has an url, maybe we reopen
      maybeReopenTab(tab);
    }
  });
}

async function containEbay (options) {
  // Listen to requests and open Ebay into its Container,
  // open other sites into the default tab context
  if (options.tabId === -1) {
    // Request doesn't belong to a tab
    return;
  }
  if (tabsWaitingToLoad[options.tabId]) {
    // Cleanup just to make sure we don't get a race-condition with startup reopening
    delete tabsWaitingToLoad[options.tabId];
  }

  // We have to check with every request if the requested URL is assigned with MAC
  // because the user can assign URLs at any given time (needs MAC Events)
  const macAssigned = await getMACAssignment(options.url);
  if (macAssigned) {
    // This URL is assigned with MAC, so we don't handle this request
    return;
  }

  const tab = await browser.tabs.get(options.tabId);
  if (tab.incognito) {
    // We don't handle incognito tabs
    return;
  }

  // Check whether we should contain this request into another container
  const cookieStoreId = shouldContainInto(options.url, tab);
  if (!cookieStoreId) {
    // Request doesn't need to be contained
    return;
  }
  if (shouldCancelEarly(tab, options)) {
    // We need to cancel early to prevent multiple reopenings
    return {cancel: true};
  }
  // Decided to contain
  reopenTab({
    url: options.url,
    tab,
    cookieStoreId
  });
  return {cancel: true};
}

(async function init () {
  await setupMACAddonListeners();
  macAddonEnabled = await isMACAddonEnabled();

  try {
    await setupContainer();
  } catch (error) {
    // TODO: Needs backup strategy
    // See https://github.com/mozilla/contain-facebook/issues/23
    // Sometimes this add-on is installed but doesn't get a ebayCookieStoreId ?
    // eslint-disable-next-line no-console
    console.log(error);
    return;
  }
  clearEbayCookies();
  generateEbayHostREs();

  // Clean up canceled requests
  browser.webRequest.onCompleted.addListener((options) => {
    if (canceledRequests[options.tabId]) {
      delete canceledRequests[options.tabId];
    }
  },{urls: ["<all_urls>"], types: ["main_frame"]});
  browser.webRequest.onErrorOccurred.addListener((options) => {
    if (canceledRequests[options.tabId]) {
      delete canceledRequests[options.tabId];
    }
  },{urls: ["<all_urls>"], types: ["main_frame"]});

  // Add the request listener
  browser.webRequest.onBeforeRequest.addListener(containEbay, {urls: ["<all_urls>"], types: ["main_frame"]}, ["blocking"]);

  maybeReopenAlreadyOpenTabs();
})();
