# Ebay Container

**Prevent Ebay from tracking your visits to other websites**

Ebay Container is an add-on you can install on Firefox to prevent Ebay from tracking your activity on other websites, so you can continue to use Ebay while protecting your privacy.

**Note:** To learn more about Containers in general, see [Firefox Multi-Account Containers](https://support.mozilla.org/kb/containers).

## How does Ebay Container work?

The Add-on keeps Ebay in a separate Container to prevent it from following your activity on other websites. When you first install the add-on, it signs you out of Ebay and deletes the cookies that Ebay uses to track you on other websites. 

Every time you visit Ebay, it will open in its own container, separate from other websites you visit.  You can login to Ebay within its container.  When browsing outside the container, Ebay won’t be able to easily collect your browsing data and connect it to your Ebay identity.

## How do I enable Ebay Container?

We’ve made it easy to take steps to protect your privacy so you can go on with your day.

1. [Install Ebay Container](https://addons.mozilla.org/firefox/addon/ebay-container/). This will log you out of Ebay and delete the cookies it’s been using to track you.
2. Open Ebay and use it like you normally would.  Firefox will automatically switch to the Ebay Container tab for you.
3. If you click on a link to a page outside of Ebay or type in another website in the address bar, Firefox will load them outside of the Ebay Container

## How does this affect Ebay’s features?

Ebay Containers prevents Ebay from linking your activity on other websites to your Ebay identity. Therefore, the following will not work:

### “Like” buttons and embedded Ebay comments on other websites.

Because you are logged into Ebay only in the Container, “Like” buttons and embedded Ebay comments on other websites will not work.

### Logging in or creating accounts on other websites using Ebay

Websites that allow you to create an account or log in using Ebay will generally not work properly.

## Will this protect me from Ebay completely?

This add-on does not prevent Ebay from mishandling the data it already has or permitted others to obtain about you. Ebay still will have access to everything that you do while you are on ebay.com or on the Ebay app, including your Ebay comments, photo uploads, likes, and any data you share with Ebay connected apps, etc.  

Other ad networks may try to link your Ebay activities with your regular browsing. In addition to this add-on, there are other things you can do to maximize your protection, including changing your Ebay settings, using Private Browsing and Tracking Protection, blocking third-party cookies, and/or using [Firefox Multi-Account Containers](https://addons.mozilla.org/firefox/addon/multi-account-containers/ ) extension to further limit tracking.

## How do I use Containers for other websites?

Good news! Containers aren’t just for Ebay. You can use Containers to prevent websites from linking your identities across the Web by installing [Firefox Multi-Account Containers](https://addons.mozilla.org/firefox/addon/multi-account-containers/).

To learn more about how Mult-Account Containers work, see our support page at [Firefox Multi-Account Containers](https://addons.mozilla.org/firefox/addon/multi-account-containers/).
